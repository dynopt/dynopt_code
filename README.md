# README #

dynopt is a set of MATLAB functions for determination of optimal control trajectory by given description of the process, the cost to be
minimised, subject to equality and inequality constraints, using orthogonal collocation on finite elements method. 

Development of the package has been moved to github: https://github.com/miroslavfikar/dynopt_code

Contact: miroslav.fikar@stuba.sk